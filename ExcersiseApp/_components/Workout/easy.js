import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, FlatList, ImageBackground, Dimensions, TouchableOpacity } from 'react-native';
import { Container, Header, Content, List, ListItem, Thumbnail, Text, Left, Body, Right, Button } from 'native-base';
const { width, height } = Dimensions.get('window');
import { Firebase } from '../Firebase';
import Icon from 'react-native-vector-icons/Ionicons';
export default class Easy extends Component {
  constructor(props) {
    super(props);
    this.state = {
      datasource: null
    };

  }
  componentDidMount() {
    this.GetData();
  }
  GetData() {
    Firebase.database().ref().child('exercises').on('value', (snap) => {
      this.setState({
        datasource: snap.val().filter(item => item.levelId === 0)
      })
    });
  }

  render() {
    return (
      <Container>
        <FlatList
          data={this.state.datasource}
          refreshing={false}
          renderItem={({ item }) =>
            <ListItem thumbnail>
              <Left>
                <Thumbnail square source={require('../../_assets/'+item.image)} width='200' height='200' />
              </Left>
              <Body>
                <Text>{item.name}</Text>
                <Text note numberOfLines={1}>{item.instuction}</Text>
              </Body>
              <Right>
                <TouchableOpacity transparent>
                  <Icon name={'ios-list'} size={28}></Icon>
                </TouchableOpacity>
              </Right>
            </ListItem>}
          keyExtractor={(item, index) => index.toString()}>
        </FlatList>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  backgroundConatiner: {
    flex: 1,
    alignItems: 'center'
  },
  background_card: {
    width: 350,
    height: 250,
    alignItems: 'flex-start',
    justifyContent: 'flex-end',
    padding: 15
  },
  gradient_card: {
    position: 'absolute',
    padding: 15,
    left: 0,
    right: 0,
    bottom: 0,
    height: height * 0.23,
    alignItems: 'flex-start',
    justifyContent: 'flex-end'
  },
  category_card: {
    color: '#f39c12',
    marginBottom: 3,
    fontSize: 20
  },
  title_card: {
    color: '#FFF',
    fontSize: 22,
    marginBottom: 3,
    fontWeight: 'bold'
  },
  subcategory_card: {
    color: '#FFF',
    fontSize: 20,
    opacity: 0.8
  },
  button: {
    marginTop: 10
  }
});
