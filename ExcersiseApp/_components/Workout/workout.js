import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Image,
  Dimensions,
  ScrollView,
  YellowBox
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { Container, Header, Content, List, ListItem, Text, Left, Thumbnail, Body, Right, Switch } from 'native-base';

const { width: WIDTH, height: HEIGHT } = Dimensions.get('window');
import Icon from 'react-native-vector-icons/Ionicons';
import { Firebase } from '../Firebase'




class Workout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      levels: [{
        id: 0,
        name: 'Beginner'
      }]
    };
    this.initData();
  }

  initData() {
    Firebase.database().ref().child('levels/').on('value', snap => {
      // const levels = []
      // snap.forEach((item) => {
      //   levels.push({
      //     id: item.val().id,
      //     name: item.val().name
      //   });
      // });
      // this.setState({
      //   levels: levels
      // });
    });
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container_menu}>
        <View style={styles.imageContainer}>
          <Image
            source={require('../../_assets/beginer.jpg')}
            style={{ flex: 1, width: WIDTH, height: HEIGHT * 0.3 }} >
          </Image>
          <Text style={styles.textTitle}>WORKOUT</Text>
        </View>


        <Grid>
          <Row style={{ height: 70, backgroundColor: '#fafafa' }}>
            <Col style={styles.col_workout}>
              <Text style={styles.titlecol_workout}>LEVEL</Text>
            </Col>
          </Row>
          <Row>
            <ScrollView>
              <ListItem style={styles.item_menu} onPress={() => navigate('Beginner', {})}>
                <Body>
                  <Text style={styles.text_menu}>Beginner</Text>
                </Body>
                <Right>
                  <Icon name="ios-arrow-forward" style={styles.icon_menu} />
                </Right>
              </ListItem>
              <ListItem style={styles.item_menu} onPress={() => navigate('Intermediate', {})}>
                <Body>
                  <Text style={styles.text_menu}>Intermediate</Text>
                </Body>
                <Right>
                  <Icon name="ios-arrow-forward" style={styles.icon_menu} />
                </Right>
              </ListItem>
              <ListItem style={styles.item_menu} onPress={() => navigate('Advance', {})}>
                <Body>
                  <Text style={styles.text_menu}>Advance</Text>
                </Body>
                <Right>
                  <Icon name="ios-arrow-forward" style={styles.icon_menu} />
                </Right>
              </ListItem>
            </ScrollView>
          </Row>
        </Grid>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container_menu: {
    flex: 1,
    backgroundColor: '#FFF'
  },
  imageContainer: {
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'flex-start',
    height: HEIGHT * 0.30,
    position: 'relative',
    zIndex: 1
  },
  textTitle: {
    position: "absolute",
    zIndex: 2,
    fontSize: 30,
    color: 'gold',
  },
  item_menu: {
    width: WIDTH - 55,
    height: 70,
    borderRadius: 25,
    backgroundColor: 'rgb(224,224,224)',
    justifyContent: "center",
    margin: 20,
    alignSelf: 'center'
  },

  text_menu: {
    fontSize: 20,
    alignSelf: 'center'
  },


  icon_menu: {
    fontSize: 20,
  },
  titlecol_workout: {
    fontWeight: 'bold',
    fontSize: 18,
    color: "#f39c12"

  },
  col_workout: {
    height: 70,
    alignItems: 'center',
    justifyContent: 'center'

  },
})

export default Workout;
