import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TextInput,
    Dimensions,
    TouchableOpacity,
    Alert
} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import { Firebase } from '../Firebase';

const { width: WIDTH } = Dimensions.get('window');

export default class register extends Component {

    constructor(props) {
        super(props);
        this.state = {
            show: true,
            press: false,
            name: "",
            role: "Customer",
            email: "",
            password: "",
            errorMessage: null
        };
    }

    showPass = () => {
        if (!this.state.press) this.setState({ show: false, press: true })
        else this.setState({ show: true, press: false })
    }
    register = () => {
        Firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.password)
            .then(() => {
                Alert.alert(
                    'Register',
                    'Registeration successes',
                    [
                        { text: 'OK', onPress: () => this.props.navigation.navigate('Login') },
                        {
                            text: 'Cancel',
                            onPress: () => console.log('Cancel Pressed'),
                            style: 'cancel',
                        },
                    ],
                    { cancelable: true })

            })
            .catch(function (error) {
                Alert.alert('Register', 'Registeration fails')
            });
        this.setState({
            email: '',
            password: '',
            name: '',
            errorMessage: null
        })
    }

    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.backgroundContainer}>
                <View style={styles.inputContainer}>
                    <Text style={styles.Title}>
                        Register
                    </Text>
                </View>
                <View>
                    <Icon name={'ios-mail'} style={styles.inputIcon} size={26} color={'black'} />
                    <TextInput
                        style={styles.input}
                        placeholder="Email Address"
                        onChangeText={(email) => this.setState({ email: email })}
                        value={this.state.email}>
                    </TextInput>
                </View>
                <View style={styles.inputContainer}>
                    <Icon name={'ios-key'} style={styles.inputIcon} size={26} color={'black'} />
                    <TextInput
                        style={styles.input}
                        secureTextEntry={this.state.show}
                        placeholder="Password"
                        onChangeText={(password) => this.setState({ password: password })}
                        value={this.state.password}>
                    </TextInput>
                    <TouchableOpacity style={styles.btn} onPress={this.showPass.bind(this)}>
                        <Icon name={this.state.press ? 'ios-eye' : 'ios-eye-off'} size={26} color={'black'}></Icon>
                    </TouchableOpacity>
                </View>

                <TouchableOpacity style={styles.btnLogin} onPress={() => this.register()}>
                    <Text style={styles.text}>Register</Text>
                </TouchableOpacity>
                <Text>Do you have an account?</Text>
                <TouchableOpacity onPress={() => navigate('Login', {})}>
                    <Text style={{ fontSize: 20, color: 'blue' }}>Login</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    backgroundContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',

    },
    Logo: {
        width: 120,
        height: 120,
        marginBottom: 50
    },
    Title: {
        fontWeight: 'bold',
        fontSize: 50,
    },
    inputContainer: {
        margin: 10
    },
    input: {
        width: WIDTH - 55,
        height: 45,
        borderRadius: 25,
        fontSize: 20,
        paddingLeft: 45,
        backgroundColor: 'rgb(224,224,224)',
        marginHorizontal: 25,
        position: "relative",
        zIndex: 1
    },
    inputIcon: {
        position: "absolute",
        left: 40,
        top: 8,
        zIndex: 2
    },
    btn: {
        position: "absolute",
        right: 50,
        top: 12,
        zIndex: 2
    },
    btnLogin: {
        width: WIDTH - 55,
        height: 45,
        borderRadius: 25,
        backgroundColor: '#432577',
        justifyContent: "center",
        marginTop: 20
    },
    text: {
        fontSize: 20,
        textAlign: "center",
        color: 'white'
    }
})

