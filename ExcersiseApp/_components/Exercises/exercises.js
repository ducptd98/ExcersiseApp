import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  ImageBackground,
  ScrollView,
  Image,
  TouchableOpacity
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { Container, Header, Content, List, ListItem, Text, Left, Thumbnail, Body, Right, Switch } from 'native-base';


const { width: WIDTH, height: HEIGHT } = Dimensions.get('window');
import Icon from 'react-native-vector-icons/Ionicons'


export default class Exercises extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }


  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container_menu}>
        <View style={styles.imageContainer}>
          <Image
            source={require('../../_assets/Pull_Up_Alt1-1440x900.jpg')}
            style={{ flex: 1, width: WIDTH, height: HEIGHT * 0.3 }} >
          </Image>
          <Text style={styles.textTitle}>EXERCISES</Text>
        </View>


        <Grid>
          <Row style={{ height: 70, backgroundColor: '#fafafa' }}>
            <Col style={styles.col_workout}>
              <Text style={styles.titlecol_workout}>LEVEL</Text>
              <Text>BEGINER</Text>
            </Col>
          </Row>
          <Row>
            <ScrollView>
              <ListItem style={styles.item_menu} onPress={() => navigate('Day1', {})}>
                <Body>
                  <Text style={styles.text_menu}>Day 1</Text>
                </Body>
                <Right>
                  <Icon name="ios-arrow-forward" style={styles.icon_menu} />
                </Right>
              </ListItem>

              <ListItem style={styles.item_menu} onPress={() => navigate('Day2', {})}>
                <Body>
                  <Text style={styles.text_menu}>Day 2</Text>
                </Body>
                <Right>
                  <Icon name="ios-arrow-forward" style={styles.icon_menu} />
                </Right>
              </ListItem>

              <ListItem style={styles.item_menu} onPress={() => navigate('Day3', {})}>
                <Body>
                  <Text style={styles.text_menu}>Day 3</Text>
                </Body>
                <Right>
                  <Icon name="ios-arrow-forward" style={styles.icon_menu} />
                </Right>
              </ListItem>

              <ListItem style={styles.item_menu} onPress={() => navigate('Day4', {})}>
                <Body>
                  <Text style={styles.text_menu}>Day 4</Text>
                </Body>
                <Right>
                  <Icon name="ios-arrow-forward" style={styles.icon_menu} />
                </Right>
              </ListItem>

              <ListItem style={styles.item_menu} onPress={() => navigate('Day5', {})}>
                <Body>
                  <Text style={styles.text_menu}>Day 5</Text>
                </Body>
                <Right>
                  <Icon name="ios-arrow-forward" style={styles.icon_menu} />
                </Right>
              </ListItem>

              <ListItem style={styles.item_menu} onPress={() => navigate('Day6', {})}>
                <Body>
                  <Text style={styles.text_menu}>Day 6</Text>
                </Body>
                <Right>
                  <Icon name="ios-arrow-forward" style={styles.icon_menu} />
                </Right>
              </ListItem>


              <ListItem style={styles.item_menu} onPress={() => navigate('Day7', {})}>
                <Body>
                  <Text style={styles.text_menu}>Day 7</Text>
                </Body>
                <Right>
                  <Icon name="ios-arrow-forward" style={styles.icon_menu} />
                </Right>
              </ListItem>
            </ScrollView>
          </Row>
        </Grid>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container_menu: {
    flex: 1,
    backgroundColor: '#FFF'
  },
  imageContainer: {
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'flex-start',
    height: HEIGHT * 0.30,
    position: 'relative',
    zIndex: 1
  },
  textTitle: {
    position: "absolute",
    zIndex: 2,
    fontSize: 30,
    color: 'gold',
  },
  item_menu: {
    borderBottomWidth: 2,
    borderBottomColor: '#f7f8f9',
    marginLeft: 0,
    paddingRight: 50,
    paddingLeft: 50,
  },

  text_menu: {
    fontSize: 18
  },


  icon_menu: {
    fontSize: 20,
  },
  titlecol_workout: {
    fontWeight: 'bold',
    fontSize: 18,
    color: "#f39c12"

  },
  col_workout: {
    height: 70,
    alignItems: 'center',
    justifyContent: 'center'

  },
});
