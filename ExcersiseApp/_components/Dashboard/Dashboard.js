import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  ImageBackground
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { Container, Header, Content, List, ListItem, Text, Left, Thumbnail, Body, Right, Switch } from 'native-base';

const { width: WIDTH, height: HEIGHT } = Dimensions.get('window');
import Icon from 'react-native-vector-icons/Ionicons'

export default class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
    }; 
  }

  render() {
    return (
      <ImageBackground source={require('../../_assets/bg.jpg')} style={{ width: '100%', height: '100%' }}>
        <Grid style={styles.container}>
          <Row style={styles.rowContainer}>
            <Col style={styles.colContainer}>
              <Text style={styles.number}>19</Text>
              <Text style={styles.text}> EXERCISES</Text>
              <Icon name={'md-alarm'} size={30} color={'white'}></Icon>
            </Col>
            <Col style={styles.colContainer}>
              <Text style={styles.number}>16</Text>
              <Text style={styles.text}> WORKOUT</Text>
              <Icon name={'md-clipboard'} size={30} color={'white'}></Icon>
            </Col>
          </Row>
          <Row style={styles.rowContainer}>
            <Col style={styles.colContainer}>
              <Text style={styles.number}>4</Text>
              <Text style={styles.text}> LEVELS</Text>
              <Icon name={'md-stats'} size={30} color={'white'}></Icon>
            </Col>
            <Col style={styles.colContainer}>
              <Text style={styles.number}>4</Text>
              <Text style={styles.text}> GOALS</Text>
              <Icon name={'md-trophy'} size={30} color={'white'}></Icon>
            </Col>
          </Row>
        </Grid>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'relative',
    zIndex: 1,

  },
  rowContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  colContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontSize: 25,
    color: 'white',
    margin: 5
  },
  number: {
    fontSize: 35,
    color: 'gold',
    margin: 5
  }
});
