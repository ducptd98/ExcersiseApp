import React, { Component } from 'react';
import {
    View,
    Text,
    Dimensions,
    StyleSheet,
    TouchableOpacity,
    Picker,
    AsyncStorage
} from 'react-native';
import Modal from 'react-native-modalbox';
import { H1, Form, Item, Input, Grid, Row, Col, Label } from 'native-base';
import DateTimePicker from "react-native-modal-datetime-picker";
import Icon from 'react-native-vector-icons/Ionicons'

const { width: WIDTH, height: HEIGHT } = Dimensions.get('window');

export default class HealthDataModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            gender: 'male',
            dateOfBirth: new Date().toLocaleDateString(),
            isDateTimePickerVisible: false,
        };
        this._retriveData();
    }

    showModal() {
        this.refs.modal.open();
    }
    closeModal() {
        this.refs.modal.close();
    }
    showDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: true });
    };
    hideDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: false });
    };
    handleDatePicked = date => {
        this.setState({ dateOfBirth: date.toLocaleDateString() })
        this.hideDateTimePicker();
    };

    _retriveData = async () => {
        try {
            const gender = await AsyncStorage.getItem('gender');
            const dateOfBirth = await AsyncStorage.getItem('dateOfBirth');

            if (gender != null) {
                this.setState({ gender: gender });
            }
            if (dateOfBirth != null) {
                this.setState({ dateOfBirth: dateOfBirth });
            }
        } catch (error) {
            console.log(error);
        }
    }

    render() {
        return (
            <Modal
                ref={'modal'}
                style={styles.contaner}
                position='center'
                backdrop={true}>
                <Grid style={styles.content}>
                    <Row>
                        <H1 style={styles.header}>HEALTH DATA</H1>
                    </Row>
                    <Row style={styles.row}>
                        <Text style={styles.text}>Gender</Text>
                        <Picker
                            selectedValue={this.state.gender}
                            onValueChange={(gender) => this.setState({ gender })}>
                            <Picker.Item label="Male" value="male" />
                            <Picker.Item label="Female" value="Female" />
                            <Picker.Item label="Unknown" value="unknown" />
                        </Picker>

                    </Row>
                    <Row style={styles.row}>
                        <Text style={styles.text}>Date of birth</Text>
                        <TouchableOpacity onPress={() => this.showDateTimePicker()}>
                            <Text style={styles.text}>{this.state.dateOfBirth}</Text>
                        </TouchableOpacity>
                        <DateTimePicker
                            isVisible={this.state.isDateTimePickerVisible}
                            onConfirm={this.handleDatePicked}
                            onCancel={this.hideDateTimePicker}
                            mode="date"
                            maximumDate = {new Date()} />
                    </Row>
                </Grid>
                <View style={styles.btnGroup}>
                    <TouchableOpacity onPress={() => this.closeModal()}>
                        <Text style={styles.cancel}>CANCEL</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {
                        this.props.parentView.setGenderAndDateofBirth(this.state.gender,this.state.dateOfBirth);
                        this.closeModal();
                    }}>
                        <Text style={styles.set}>SET</Text>
                    </TouchableOpacity>
                </View>
            </Modal>
        );
    }
}
const styles = StyleSheet.create({
    contaner: {
        borderRadius: 25,
        shadowRadius: 10,
        width: WIDTH - 80,
        height: 320,
        alignItems: 'center'
    },
    header: {
        marginTop: 50,
    },
    content: {
    },
    text: {
        fontSize: 20,
        paddingTop: 10
    },
    input: {
        fontSize: 20
    },
    row: {
        flexDirection: 'column'
    },
    btnGroup: {
        alignSelf: 'flex-end',
        flexDirection: 'row',
        paddingRight: 20,
        paddingBottom: 20
    },
    cancel: {
        fontSize: 20,
        marginRight: 10,
        paddingTop: 20
    },
    set: {
        fontSize: 20,
        paddingTop: 20
    }
});
