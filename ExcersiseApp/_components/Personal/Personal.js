import React, { Component } from 'react';
import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  AsyncStorage,
  Picker
} from 'react-native';
import { Firebase } from '../Firebase';
import { Button, Switch, H2, H1 } from 'native-base';
import DateTimePicker from "react-native-modal-datetime-picker";
import Icon from 'react-native-vector-icons/Ionicons'
import Modal from 'react-native-modalbox';
import CountdownModal from './CountdownModal';
import HealthDataModal from './HealthDataModal';
import MetricUnitModal from './MetricUnitModal';

const { width: WIDTH, height: HEIGHT } = Dimensions.get('window');

export default class Personal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      syncGoogleFit: false,
      isDateTimePickerVisible: false,
      time: new Date().toLocaleTimeString(),
      countdownTime: 10,
      gender: '',
      dateOfBirth: null,
      height: 0,
      weight: 0
    };
    this._isMounted = false; // cancel all subcription
    this._retriveData();
    // this.openCountdownModel = this.openCountdownModel.bind(this);
  }

  _storeData = async () => {
    try {
      await AsyncStorage.setItem('savedTime', this.state.time);
      await AsyncStorage.setItem('countdownTime', this.state.countdownTime.toString());
      await AsyncStorage.setItem('gender', this.state.gender);
      await AsyncStorage.setItem('dateOfBirth', this.state.dateOfBirth);
      await AsyncStorage.setItem('weight', this.state.weight);
      await AsyncStorage.setItem('height', this.state.height);
    } catch (error) {
      console.log(error);
    }
  }
  _retriveData = async () => {
    try {
      const time = await AsyncStorage.getItem('savedTime');
      const countdownTime = await AsyncStorage.getItem('countdownTime');

      if (time != null) {
        this.setState({ time: time });
      }
      if (countdownTime != null) {
        this.setState({ countdownTime: parseInt(countdownTime) });
      }

    } catch (error) {
      console.log('retrieving data failed')
    }
  }
  componentDidMount() {
    this._isMounted = true;
  }
  componentWillUnmount() {
    this._isMounted = false;
  }

  showDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: true });
  };
  hideDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: false });
  };
  handleDatePicked = date => {
    this.setState({ time: date.toLocaleTimeString() })
    this._storeData();
    this.hideDateTimePicker();
  };
  logout() {
    Firebase.auth().signOut();
  }
  onSwitchChange() {
    const changed = this.state.syncGoogleFit;
    this.setState({ syncGoogleFit: !changed });
  }

  openCountdownModel() {
    this.refs.countdown.showModal();
  }
  setCountdownTime(time) {
    this.setState({ countdownTime: time });
    this._storeData();
  }

  openHealthDataModal() {
    this.refs.healthdata.showModal();
  }
  setGenderAndDateofBirth(gender, dateOfBirth) {
    this.setState({ gender: gender, dateOfBirth: dateOfBirth });
    this._storeData();
  }

  openMetricUnitModal() {
    this.refs.metricunit.showModal();
  }
  setMetricUnit(weight, height) {
    this.setState({ weight: weight, height: height });
    console.log(weight);
    console.log(height);
    this._storeData();

  }

  render() {
    return (
      <View style={styles.container}>
        <H2 style={styles.header}>WORKOUT</H2>
        <View style={{ borderBottomWidth: 1 }}></View>

        <TouchableOpacity style={styles.content} onPress={() => this.showDateTimePicker()}>
          <Text style={styles.name}>Remider</Text>
          <Text style={styles.value} >{this.state.time}</Text>
          <DateTimePicker
            isVisible={this.state.isDateTimePickerVisible}
            onConfirm={this.handleDatePicked}
            onCancel={this.hideDateTimePicker}
            style={styles.value}
            mode="time" />
        </TouchableOpacity>

        <TouchableOpacity style={styles.content} onPress={() => this.openCountdownModel()}>
          <Text style={styles.name}>Countdown Time</Text>
          <Text style={styles.value}>{this.state.countdownTime}</Text>
        </TouchableOpacity>

        <View style={{ borderBottomWidth: 1 }}></View>
        <H2 style={styles.header}>GENERAL SETTING</H2>

        <TouchableOpacity style={styles.content}>
          <Text style={styles.name}>Sync to Google Fit</Text>
          <Switch style={styles.value}
            value={this.state.syncGoogleFit}
            onValueChange={() => this.onSwitchChange()}></Switch>
        </TouchableOpacity>

        <TouchableOpacity style={styles.content} onPress={() => this.openHealthDataModal()}>
          <Text style={styles.name}>Health Data</Text>
          <Icon name={'md-heart'} size={28}></Icon>
        </TouchableOpacity>

        <TouchableOpacity style={styles.content} onPress={() => this.openMetricUnitModal()}>
          <Text style={styles.name}>Metric &  imperial</Text>
          <Icon name={'md-calculator'} size={28}></Icon>
        </TouchableOpacity>

        <TouchableOpacity style={styles.content}>
          <Text style={styles.name}>Restart process</Text>
          <Icon name={'md-refresh'} size={28}></Icon>
        </TouchableOpacity>

        <TouchableOpacity style={styles.content} onPress={() => this.logout()}>
          <Text style={[styles.name, { color: 'red' }]}>Log out</Text>
          <Icon name={'md-log-out'} size={28} color={'red'} />
        </TouchableOpacity>

        <CountdownModal ref={'countdown'} parentView={this}>

        </CountdownModal>
        <HealthDataModal ref={'healthdata'} parentView={this}>

        </HealthDataModal>
        <MetricUnitModal ref={'metricunit'} parentView={this}>

        </MetricUnitModal>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 20,
    height: HEIGHT
  },
  header: {
    fontSize: 26,
    color: '#339966',
    paddingBottom: 5,
    paddingTop: 5
  },
  content: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 10,
    paddingBottom: 10,
    flex: 2
  },
  value: {
    fontSize: 20,
  },
  name: {
    fontSize: 20,
  }
})
