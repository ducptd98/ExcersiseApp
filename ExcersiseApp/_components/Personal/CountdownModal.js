import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Dimensions,
    TouchableOpacity,
    AsyncStorage
} from 'react-native';
import Modal from 'react-native-modalbox';
import NumericInput from 'react-native-numeric-input';
import { H1 } from 'native-base';

const { width: WIDTH, height: HEIGHT } = Dimensions.get('window');

export default class CountdownModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 10
        };
    }

    showModal() {
        this.refs.modal.open();
    }
    closeModal() {
        this.refs.modal.close();
    }

   
    render() {
        return (
            <Modal
                ref={'modal'}
                style={styles.contaner}
                position='center'
                backdrop={true}>
                <H1 style={{ marginBottom: 10 }}>Countdown Time</H1>
                <NumericInput
                    value={this.state.value}
                    onChange={value => this.setState({ value })}
                    onLimitReached={(isMax, msg) => console.log(isMax, msg)}
                    totalWidth={240}
                    totalHeight={50}
                    iconSize={25}
                    step={1}
                    valueType='real'
                    rounded
                    textColor='#B0228C'
                    iconStyle={{ color: 'white' }}
                    rightButtonBackgroundColor='#339966'
                    leftButtonBackgroundColor='#339966'
                    maxValue={15}
                    minValue={10} />
                <View style={styles.btnGroup}>
                    <TouchableOpacity onPress={() => this.closeModal()}>
                        <Text style={styles.cancel}>CANCEL</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {
                        this.props.parentView.setCountdownTime(this.state.value);
                        this.closeModal();
                    }}>
                        <Text style={styles.set}>SET</Text>
                    </TouchableOpacity>
                </View>
            </Modal>
        );
    }
}
const styles = StyleSheet.create({
    contaner: {
        justifyContent: 'center',
        borderRadius: 25,
        shadowRadius: 10,
        width: WIDTH - 80,
        height: 200,
        alignItems: 'center'
    },
    btnGroup: {
        alignSelf: 'flex-end',
        flexDirection: 'row',
        paddingRight: 20,
    },
    cancel: {
        fontSize: 20,
        marginRight: 10,
        paddingTop: 20
    },
    set: {
        fontSize: 20,
        paddingTop: 20
    }
});
