import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';
import { H1, Grid, Row, Col, Input } from 'native-base';
import Modal from 'react-native-modalbox';


const { width: WIDTH, height: HEIGHT } = Dimensions.get('window');

export default class MetricUnitModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            height: 0,
            weight: 0
        };
    }
    showModal() {
        this.refs.modal.open();
    }
    closeModal() {
        this.refs.modal.close();
    }
    render() {
        return (
            <Modal
                ref={'modal'}
                style={styles.contaner}
                position='center'
                backdrop={true}>
                <Grid style={styles.content}>
                    <Row>
                        <H1 style={styles.header}>METRIC & IMPERIAL</H1>
                    </Row>
                    <Row style={styles.row}>
                        <Col>
                            <Text style={styles.text}>Weight</Text>
                            <Input keyboardType='numeric' maxLength={3} placeholder="Weight..." onChangeText={(weight)=>this.setState({weight})}></Input>
                        </Col>
                        <Col>
                            <Text style={styles.unit}>(kg)</Text>
                        </Col>
                    </Row>
                    <Row style={styles.row}>
                        <Col>
                            <Text style={styles.text}>Height</Text>
                            <Input keyboardType='numeric' maxLength={3} placeholder="Height..." onChangeText={(height)=>this.setState({height})}></Input>
                        </Col>
                        <Col>
                            <Text style={styles.unit}>(cm)</Text>
                        </Col>
                    </Row>
                </Grid>
                <View style={styles.btnGroup}>
                    <TouchableOpacity onPress={() => this.closeModal()}>
                        <Text style={styles.cancel}>CANCEL</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {
                        this.props.parentView.setMetricUnit(this.state.weight, this.state.height);
                        this.closeModal();
                    }}>
                        <Text style={styles.set}>SET</Text>
                    </TouchableOpacity>
                </View>
            </Modal>
        );
    }
}
const styles = StyleSheet.create({
    contaner: {
        borderRadius: 25,
        shadowRadius: 10,
        width: WIDTH - 80,
        height: 320,
        alignItems: 'center'
    },
    header: {
        marginTop: 50,
    },
    content: {
    },
    text: {
        fontSize: 20,
        paddingTop: 10
    },
    unit: {
        fontSize: 20,
        paddingTop: 45
    },
    input: {
        fontSize: 20
    },
    row: {
    },
    btnGroup: {
        alignSelf: 'flex-end',
        flexDirection: 'row',
        paddingRight: 20,
        paddingBottom: 20
    },
    cancel: {
        fontSize: 20,
        marginRight: 10,
        paddingTop: 20
    },
    set: {
        fontSize: 20,
        paddingTop: 20
    }
});
