import React, { Component } from 'react';
import {
  View,
  Text,
  Button,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Image,
  Alert
} from 'react-native';
const { width: WIDTH, height: HEIGHT } = Dimensions.get('window');
import Icon from 'react-native-vector-icons/Ionicons'
import RCTCamera, { RNCamera } from 'react-native-camera';




class CheckFood extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null
    };
  }
  onBarCodeRead = (e) => {
    try {
      fetch('https://world.openfoodfacts.org/api/v0/product/' + e.data + '.json',
        {
          method: 'GET',
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json"
          }
        })
        .then((response) => response.json())
        .then((responseJson) => {
          result = JSON.stringify(responseJson)
          Alert.alert(result)
          // this.setState({data: result['product']['nutrition_grades']})
        })
    } catch (error) {
      Alert.alert('Error',error.messages)
    }
    Alert.alert('HERRE')
    // navigate("checkfood", data)
  }
  // onPress={() => navigation.navigate('Barcode', {})}
  render() {
    const maskRowHeight = Math.round((HEIGHT - 300) / 20);
    const maskColWidth = (WIDTH - 300) / 2;
    // const { navigation } = this.props;
    return (

      <View style={styles.container}>
        <RNCamera
          ref={cam => {
            this.camera = cam;
          }}
          onBarCodeRead={this.onBarCodeRead}
          style={styles.cameraView}
          playSoundOnCapture = {true}
          onGoogleVisionBarcodesDetected = {
            (e)=> console.warn(e)
          }
        >
          <View style={styles.maskOutter}>
            <View style={[{ flex: maskRowHeight }, styles.maskRow, styles.maskFrame]} />
            <View style={[{ flex: 30 }, styles.maskCenter]}>
              <View style={[{ width: maskColWidth }, styles.maskFrame]} />
              <View style={styles.maskInner} />
              <View style={[{ width: maskColWidth }, styles.maskFrame]} />
            </View>
            <View style={[{ flex: maskRowHeight }, styles.maskRow, styles.maskFrame]} />
          </View>
        </RNCamera>
      </View>
    );
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  cameraView: {
    flex: 1,
    justifyContent: 'flex-start',
  },
  maskOutter: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  maskInner: {
    width: 300,
    backgroundColor: 'transparent',
    borderColor: 'white',
    borderWidth: 1,
  },
  maskFrame: {
    backgroundColor: 'rgba(1,1,1,0.6)',
  },
  maskRow: {
    width: '100%',
  },
  maskCenter: { flexDirection: 'row' },
});


export default CheckFood;
