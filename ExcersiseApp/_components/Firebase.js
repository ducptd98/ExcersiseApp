import * as firebase from 'firebase';
import "firebase/firestore"

const firebaseConfig = {
    apiKey: "AIzaSyAHF7FpAyXmkDnZG-hhms7OzGz2rVs6V4s",
    authDomain: "fitnessapp-e7591.firebaseapp.com",
    databaseURL: "https://fitnessapp-e7591.firebaseio.com",
    projectId: "fitnessapp-e7591",
    storageBucket: "fitnessapp-e7591.appspot.com",
    messagingSenderId: "438101608927",
    appId: "1:438101608927:web:4c6def2f8b50683ec4dc6a",
    measurementId: "G-6T0C5V70N5"
};

export const Firebase = firebase.initializeApp(firebaseConfig)
