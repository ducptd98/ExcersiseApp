
import React, { Component } from 'react';
import { View, Text, ImageBackground, Dimensions, StyleSheet, ActivityIndicator } from 'react-native';
import { Firebase } from '../Firebase';

const { width: WIDTH, height: HEIGHT } = Dimensions.get('window');

export default class Loading extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    componentDidMount() {
        setTimeout(() => {
            Firebase.auth().onAuthStateChanged((user) => {
                this.props.navigation.navigate(user ? 'Home' : 'Login');
            })
        }, 2000);
        clearTimeout();
    }
    render() {
        return (
            <View style={styles.container}>
                <ImageBackground source={require('../../_assets/bg.jpg')}
                    style={{ width: WIDTH, height: HEIGHT }}>
                    <ActivityIndicator size={70}
                        color="gold" style={styles.loading}
                    />
                </ImageBackground>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    loading: {
        marginTop: '75%'
    }
})
