import Exercises from '../_components/Exercises/exercises';
import Day1 from '../_components/Exercises/Day1';
import Day2 from '../_components/Exercises/Day2';
import Day3 from '../_components/Exercises/Day3';
import Day4 from '../_components/Exercises/Day4';
import Day5 from '../_components/Exercises/Day5';
import Day6 from '../_components/Exercises/Day6';
import Day7 from '../_components/Exercises/Day7';


import { createStackNavigator, } from 'react-navigation-stack';

export default ExercisesNavigator = createStackNavigator({
    Exercises: {
        screen: Exercises,
        navigationOptions:{
            header: null
        },
        path: 'exercises'
    },
    Day1: {
        screen: Day1,
        path: 'day1'
    },
    Day2: {
        screen: Day2,
        path: 'day2'
    },
    Day3: {
        screen: Day3,
        path: 'day3'
    },
    Day4: {
        screen: Day4,
        path: 'day4'
    },
    Day5: {
        screen: Day5,
        path: 'day5'
    },
    Day6: {
        screen: Day6,
        path: 'day6'
    },
    Day7: {
        screen: Day7,
        path: 'day7'
    },
},{
    initialRouteName: 'Exercises'
})