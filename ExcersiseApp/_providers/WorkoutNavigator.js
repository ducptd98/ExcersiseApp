import Easy from '../_components/Workout/easy';
import Medium from '../_components/Workout/medium';
import Hard from '../_components/Workout/hard'
import Workout from '../_components/Workout/workout'

import { createStackNavigator, } from 'react-navigation-stack';


export default WorkoutNavigator = createStackNavigator({
    Workout: {
        screen: Workout,
        navigationOptions: {
            header: null
        },
        path: 'workout'
    },
    Beginner: {
        screen: Easy,
        navigationOptions: {
            title: 'Level Beginner',
        },
        path: 'easy'
    },
    Intermediate: {
        screen: Medium,
        navigationOptions: {
            title: 'Level Intermediate',
        },
        path: 'medium'
    },
    Advance: {
        screen: Hard,
        navigationOptions: {
            title: 'Level Advance',
        },
        path: 'hard',
    }
},{
    initialRouteName: 'Workout'
});