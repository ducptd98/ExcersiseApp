const ConfigApp = {

    // backend url
    URL: "YOUR_BACKEND_URL",

    // banner admob unit id
    BANNER_ID: "YOUR_BANNER_ID",

    // interstitial admob unit id
    INTERSTITIAL_ID: "YOUR_INTERSTITIAL_ID",
};

export default ConfigApp;