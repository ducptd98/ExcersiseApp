import { createAppContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createStackNavigator } from 'react-navigation-stack';
import Icon from 'react-native-vector-icons/Ionicons'
import React from 'react';

import Dashboard from './_components/Dashboard/Dashboard';
import Login from './_components/Login/login';
import register from './_components/Register/register';
import CheckFood from './_components/CheckFood/check-food';
import WorkoutNavigator from './_providers/WorkoutNavigator';
import Personal from './_components/Personal/Personal';
import ExerciseNavigator from './_providers/ExerciseNavigator';
import Loading from './_components/Loading/Loading';


const Tabs = createBottomTabNavigator({
  Dashboard: {
    screen: Dashboard,
    navigationOptions: {
      title: 'Dash Board',
      tabBarLabel: 'Dash Board',
      tabBarIcon: ({ tintColor }) => (
        <Icon name={'ios-clipboard'} size={26} color={'black'} />
      ),
    },
    path: 'dashboard'
  },
  CheckFood: {
    screen: CheckFood,
    navigationOptions: {
      title: 'Check Food',
      tabBarLabel: 'Check Food',
      tabBarIcon: ({ tintColor }) => (
        <Icon name={'ios-search'} size={26} color={'black'} />
      ),
    },
    path: 'checkfood'
  },
  Exercise: {
    screen: ExerciseNavigator,
    navigationOptions: {
      title: 'Exercises',
      tabBarLabel: 'Exercises',
      tabBarIcon: ({ tintColor }) => (
        <Icon name={'ios-fitness'} size={26} color={'black'} />
      ),
    },
    path: 'exercise'
  },
  Workout: {
    screen: WorkoutNavigator,
    navigationOptions: {
      title: 'Work Out',
      tabBarLabel: 'Work Out',
      tabBarIcon: ({ tintColor }) => (
        <Icon name={'ios-calendar'} size={26} color={'black'} />
      ),
    },
    path: 'workout'
  },
  Personal: {
    screen: Personal,
    navigationOptions: {
      title: 'ME',
      tabBarLabel: 'ME',
      tabBarIcon: ({ tintColor }) => (
        <Icon name={'ios-person'} size={26} color={'black'} />
      ),
      
    },
    path: 'personal',
  },
},
  {
    tabBarOptions: {
      activeTintColor: 'tomato',
      inactiveTintColor: 'gray',
      style: {
        alignContent: "center",
      },

    },
  });

const MainNavigator = createStackNavigator({
  Loading:{
    screen: Loading,
    navigationOptions: {
      header: null
    }
  },
  Login: {
    screen: Login,
    navigationOptions: {
      header: null
    }
  },
  Register: {
    screen: register,
    navigationOptions: {
      header: null,
    }
  },
  Home: {
    screen: Tabs,
    navigationOptions: {
      header: null,
    },
  },
},{
  initialRouteName: 'Loading'
});


const App = createAppContainer(MainNavigator);
export default App;
